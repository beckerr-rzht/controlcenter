msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"Content-Type: \n"
"Content-Transfer-Encoding: \n"

#: www/index.i18n.html:39
msgid "<b>%i%%</b> bounce rate between %s and %s"
msgstr "<b>%i%%</b> bounce rate between %s and %s"

#: www/index.i18n.html:102 www/settingspage.i18n.html:46
msgid "About"
msgstr "About"

#: www/index.i18n.html:319
msgid "About Lightmeter"
msgstr "About Lightmeter"

#: www/index.i18n.html:270
msgid "All"
msgstr "All"

#: www/index.i18n.html:194
msgid "and welcome back"
msgstr "and welcome back"

#: www/index.i18n.html:226
msgid "Bounced domains"
msgstr "Bounced domains"

#: www/settingspage.i18n.html:104
msgid "Brazilian Portuguese"
msgstr "Brazilian Portuguese"

#: www/index.i18n.html:31
msgid "Breaking new ground"
msgstr "Breaking new ground"

#: www/index.i18n.html:223
msgid "Busiest domains"
msgstr "Busiest domains"

#: www/settingspage.i18n.html:118 www/settingspage.i18n.html:130
msgid "Cancel"
msgstr "Cancel"

#: www/index.i18n.html:122 www/index.i18n.html:142 www/index.i18n.html:162 www/settingspage.i18n.html:66
msgid "Close"
msgstr "Close"

#: www/index.i18n.html:275
msgid "Comparative"
msgstr "Comparative"

#: www/index.i18n.html:176
msgid "Control Center"
msgstr "Control Center"

#: www/index.i18n.html:229
msgid "Deferred domains"
msgstr "Deferred domains"

#: www/index.i18n.html:210
msgid "Delivery attempts"
msgstr "Delivery attempts"

#: www/index.i18n.html:60 www/index.i18n.html:67
msgid "Details"
msgstr "Details"

#: www/register.i18n.html:130
msgid "Different types of mail perform differently. This helps show the most relevant information."
msgstr "Different types of mail perform differently. This helps show the most relevant information."

#: www/register.i18n.html:125
msgid "Direct (personal, office, one-to-one)"
msgstr "Direct (personal, office, one-to-one)"

#: www/login.i18n.html:69 www/register.i18n.html:110
msgid "E-mail"
msgstr "E-mail"

#: www/login.i18n.html:70 www/register.i18n.html:111
msgid "Email"
msgstr "Email"

#: www/settingspage.i18n.html:96
msgid "English"
msgstr "English"

#: www/login.i18n.html:35 www/register.i18n.html:48 www/register.i18n.html:51
msgid "Error"
msgstr "Error"

#: www/settingspage.i18n.html:185
msgid "Error on saving general settings!"
msgstr "Error on saving general settings!"

#: www/settingspage.i18n.html:171
msgid "Error on saving notification settings!"
msgstr "Error on saving notification settings!"

#: www/index.i18n.html:317 www/settingspage.i18n.html:141
msgid "Feedback"
msgstr "Feedback"

#: www/index.i18n.html:268
msgid "Filter"
msgstr "Filter"

#: www/login.i18n.html:80
msgid "Forgot password?"
msgstr "Forgot password?"

#: www/settingspage.i18n.html:121
msgid "General"
msgstr "General"

#: www/settingspage.i18n.html:100
msgid "German"
msgstr "German"

#: www/register.i18n.html:97
msgid "Get help"
msgstr "Get help"

#: www/index.i18n.html:75
msgid "Happy %s,"
msgstr "Happy %s,"

#: www/index.i18n.html:28
msgid "High Bounce Rate"
msgstr "High Bounce Rate"

#: www/index.i18n.html:298 www/index.i18n.html:298
msgid "Info"
msgstr "Info"

#: www/index.i18n.html:256
msgid "Insights"
msgstr "Insights"

#: www/index.i18n.html:49
msgid "Insights are time-based analyses relating to your mailserver"
msgstr "Insights are time-based analyses relating to your mailserver"

#: www/index.i18n.html:274
msgid "Intel"
msgstr "Intel"

#: www/index.i18n.html:33
msgid "IP blocked by %s"
msgstr "IP blocked by %s"

#: www/index.i18n.html:32
msgid "IP on shared blocklist"
msgstr "IP on shared blocklist"

#: www/index.i18n.html:54
msgid "Keep Control Center running to generate Insights; checks run every few seconds"
msgstr "Keep Control Center running to generate Insights; checks run every few seconds"

#: www/index.i18n.html:108 www/settingspage.i18n.html:52
msgid "Lightmeter Control Center"
msgstr "Lightmeter Control Center"

#: www/index.i18n.html:22 www/settingspage.i18n.html:22
msgid "Lightmeter ControlCenter"
msgstr "Lightmeter ControlCenter"

#: www/index.i18n.html:271
msgid "Local"
msgstr "Local"

#: www/login.i18n.html:19 www/login.i18n.html:63 www/login.i18n.html:82
msgid "Login"
msgstr "Login"

#: www/index.i18n.html:29
msgid "Mail Inactivity"
msgstr "Mail Inactivity"

#: www/register.i18n.html:127
msgid "Marketing (newsletters, adverts)"
msgstr "Marketing (newsletters, adverts)"

#: www/register.i18n.html:138
msgid "Monthly newsletter"
msgstr "Monthly newsletter"

#: www/register.i18n.html:122 www/register.i18n.html:124
msgid "Most of my mail is…"
msgstr "Most of my mail is…"

#: www/register.i18n.html:107 www/register.i18n.html:108
msgid "Name"
msgstr "Name"

#: www/index.i18n.html:279
msgid "Newest"
msgstr "Newest"

#: www/index.i18n.html:272
msgid "News"
msgstr "News"

#: www/index.i18n.html:116 www/settingspage.i18n.html:60
msgid "Newsletter"
msgstr "Newsletter"

#: www/settingspage.i18n.html:89
msgid "No"
msgstr "No"

#: www/index.i18n.html:44
msgid "No emails were sent between %s and %s"
msgstr "No emails were sent between %s and %s"

#: www/settingspage.i18n.html:79
msgid "Notifications"
msgstr "Notifications"

#: www/index.i18n.html:280
msgid "Oldest"
msgstr "Oldest"

#: www/index.i18n.html:364
msgid "Original response from %s (%s)"
msgstr "Original response from %s (%s)"

#: www/login.i18n.html:73 www/login.i18n.html:74 www/register.i18n.html:114 www/register.i18n.html:115
msgid "Password"
msgstr "Password"

#: www/register.i18n.html:96
msgid "Please create a new administrator account - this is necessary to login."
msgstr "Please create a new administrator account - this is necessary to login."

#: www/settingspage.i18n.html:113
msgid "Please enter api token"
msgstr "Please enter api token"

#: www/settingspage.i18n.html:109
msgid "Please enter slack channel name"
msgstr "Please enter slack channel name"

#: www/settingspage.i18n.html:126
msgid "Please extract from your config file the ip address of postfix"
msgstr "Please extract from your config file the ip address of postfix"

#: www/register.i18n.html:30
msgid "Please select an option for 'Most of my mail is' - see help for details"
msgstr "Please select an option for 'Most of my mail is' - see help for details"

#: www/settingspage.i18n.html:124
msgid "Postfix public IP"
msgstr "Postfix public IP"

#: www/index.i18n.html:319 www/settingspage.i18n.html:143
msgid "Privacy Policy"
msgstr "Privacy Policy"

#: www/index.i18n.html:133
msgid "RBLs for"
msgstr "RBLs for"

#: www/index.i18n.html:357
msgid "RBLs for "
msgstr "RBLs for "

#: www/index.i18n.html:319
msgid "Read policy"
msgstr "Read policy"

#: www/register.i18n.html:141
msgid "Register"
msgstr "Register"

#: www/register.i18n.html:19
msgid "Registration"
msgstr "Registration"

#: www/settingspage.i18n.html:117 www/settingspage.i18n.html:129
msgid "Save"
msgstr "Save"

#: www/settingspage.i18n.html:189
msgid "Saved general settings"
msgstr "Saved general settings"

#: www/settingspage.i18n.html:175
msgid "Saved notification settings"
msgstr "Saved notification settings"

#: www/login.i18n.html:30 www/login.i18n.html:41 www/register.i18n.html:40 www/register.i18n.html:73
msgid "Server Error!"
msgstr "Server Error!"

#: www/settingspage.i18n.html:77
msgid "Settings"
msgstr "Settings"

#: www/register.i18n.html:69
msgid "Settings Error on initial setup!"
msgstr "Settings Error on initial setup!"

#: www/settingspage.i18n.html:112
msgid "Slack API token"
msgstr "Slack API token"

#: www/settingspage.i18n.html:108
msgid "Slack channel"
msgstr "Slack channel"

#: www/settingspage.i18n.html:93
msgid "Slack message language"
msgstr "Slack message language"

#: www/settingspage.i18n.html:82
msgid "Slack notifications"
msgstr "Slack notifications"

#: www/index.i18n.html:319 www/settingspage.i18n.html:143
msgid "Thank you for using Lightmeter."
msgstr "Thank you for using Lightmeter."

#: www/index.i18n.html:66
msgid "The IP %s cannot deliver to %s (<strong>%s</strong>)"
msgstr "The IP %s cannot deliver to %s (<strong>%s</strong>)"

#: www/index.i18n.html:59
msgid "The IP address %s is listed by <strong>%d</strong> <abbr title='Real-time Blackhole List'>RBL</abbr>s"
msgstr "The IP address %s is listed by <strong>%d</strong> <abbr title='Real-time Blackhole List'>RBL</abbr>s"

#: www/index.i18n.html:259
msgid "Time interval:"
msgstr "Time interval:"

#: www/register.i18n.html:98
msgid "to avoid repeating this step if you've done it before"
msgstr "to avoid repeating this step if you've done it before"

#: www/register.i18n.html:126
msgid "Transactional (notifications, apps)"
msgstr "Transactional (notifications, apps)"

#: www/register.i18n.html:101
msgid "User details"
msgstr "User details"

#: www/register.i18n.html:48
msgid "Vulnerable to"
msgstr "Vulnerable to"

#: www/index.i18n.html:114 www/settingspage.i18n.html:58
msgid "Website"
msgstr "Website"

#: www/register.i18n.html:95
msgid "Welcome"
msgstr "Welcome"

#: www/index.i18n.html:317
msgid "What would you improve?"
msgstr "What would you improve?"

#: www/settingspage.i18n.html:85
msgid "Yes"
msgstr "Yes"

#: www/index.i18n.html:30
msgid "Your first Insight"
msgstr "Your first Insight"

